const { DataEntry } = require("../schema/input/dataEntry");
const { createHello } = require("../service/createHello");

// a comment

module.exports = async (commandPayload, commandMeta) => {
  new DataEntry(commandPayload, commandMeta);
  const { nombre } = commandPayload;
  await createHello({ hola: nombre });
  return { body: `Hola : ${nombre}` };
};
