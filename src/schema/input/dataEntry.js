const { InputValidation } = require("nbased/schema/inputValidation");

class DataEntry extends InputValidation {
  constructor(payload, meta) {
    super({
      type: "NB-HELLO.HELLO",
      specversion: "v1.0.0",
      source: meta.source,
      payload: payload,
      inputSchema: {
        schema: { nombre: { type: String, required: true } },
        settings: {},
      },
    });
  }
}

module.exports = { DataEntry };
