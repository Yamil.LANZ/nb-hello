const { DownstreamEvent } = require("nbased/schema/downstreamEvent");

class ExchangeCreatedEvent extends DownstreamEvent {
  constructor(payload, meta) {
    super({
      type: "NB-HELLO.HELLO_CREATED",
      specversion: "v1.0.0",
      payload: payload,
      meta: meta,
      inputSchema: {
        schema: {
          strict: false,
          id: { type: "uuid/v4", required: true },
          data: { type: String, required: true },
        },
      },
    });
  }
}

module.exports = { ExchangeCreatedEvent };
