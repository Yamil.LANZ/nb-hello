const { commandMapper } = require("nbased/handler");
const inputMode = require("nbased/handler/input/commandApi");
const outputMode = require("nbased/handler/output/commandApi");

const helloMundo = require("../domain/helloMundo");

module.exports.hello = async (command, context) => {
  return commandMapper({ command, context }, inputMode, helloMundo, outputMode);
};
