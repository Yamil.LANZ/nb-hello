const uuid = require("uuid");
const dynamo = require("nbased/service/storage/dynamo");

const createHello = async (item) => {
  item.id = uuid.v4();
  dynamo.putItem({ TableName: "nb-hello.hello", Item: item });
};
module.exports = { createHello };
